package Penugasan;

import java.util.ArrayList;
import java.util.Scanner;

// Abdul Fikri Zaki
// 235150400111007
// SI A

public class Tugas2 {

    public static void main(String[] args) {
       

        Scanner input = new Scanner(System.in);
        //mnembuat arraylist untuk list mahasiswa
        ArrayList<Mahasiswa> mahasiswaList = new ArrayList<>();
        //membuat var check untuk melanjutkan kode
        String check = "y";

        System.out.println();

        
        
        while(check.equals("y")){

            input.nextLine();
        //membuat object mahasiswa

            Mahasiswa mhs = new Mahasiswa();

        //menginput nama

            System.out.print("Nama: ");
            String nama = input.nextLine();
            mhs.setNama(nama);

        //menginput NIM

            System.out.print("NIM: ");
            String NIM = input.nextLine();
            mhs.setNim(NIM);

        //mengambil alamat

            System.out.print("Alamat: ");
            String alamat = input.nextLine();
            mhs.setAlamat(alamat);

        //menambah mahasiswa ke list

            mahasiswaList.add(mhs);

        //check apakah ingin menambah mahasiswa

            System.out.print("Ingin Menambah? (y/n)");
            String checking = input.next();

        //checking

            check = checking;
        }

        //output

        for (Mahasiswa mahasiswa : mahasiswaList) {
            System.out.println("====================================");
            System.out.printf("%-15s | %-10s | %-30s%n", mahasiswa.nama, mahasiswa.nim, mahasiswa.alamat);
            System.out.println("=====================================");
        }
        
        input.close();
    }
}


class Mahasiswa{ //Membuat kelas mahasiswa
    String nama;
    String nim;
    String alamat;

    //set nama
    void setNama(String nama){
        this.nama = nama;
    }
    //set Nim
    void setNim(String nim){
        this.nim = nim;
    }
    //set Alamat
    void setAlamat(String alamat){
        this.alamat = alamat;
    }

}